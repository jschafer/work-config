A collection of lists, things to look at later, configurations, etc.

Run the following command from this directory. **This will replace your `.aliases` file.**

```
./link_aliases.sh
```
