# what-i-use

A collection of apps, configs, peripherals, etc. I use for my dev+ environment

# initial setup
1. Install iTerm2
2. Install command line tools: `xcode-select --install`

[Rails Install Instructions](https://andrewm.codes/blog/how-to-install-ruby-on-rails-6-1-with-asdf-on-macos-big-sur/)
3. Install Homebrew: `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
4. Install asdf: `brew install coreutils curl git gpg gawk zsh yarn asdf`

# applications
* [iterm2](https://iterm2.com) - Terminal
  * Color Schemes  
    A lot of good color schemes can be found at https://github.com/mbadolato/iTerm2-Color-Schemes.  
    `git clone https://github.com/mbadolato/iTerm2-Color-Schemes.git`  
    These are my faves. I usually use a dark BG with white or green text, but use a couple to change things up (BG/FG)
    * Brogrammer (Dark/White)
    * FirefoxDev (Dark/Grey)
    * Glacier (Dark/White)
    * Homebrew (Dark/Green)
    * Ubuntu (Purple/White)
    * Red Sands (Red/Grey)
  * Config - TODO
  * .bash_aliases - TODO
* [Homebrew](https://brew.sh) - Package manager
* MacVim - Because sometimes you just want a simple editor.
  * `brew install macvim`
* [VSCode](https://code.visualstudio.com/)
  * [Extensions](https://dev.to/thomasvanholder/10-vs-code-extensions-for-ruby-on-rails-developers-89a) 
* [Spotify](https://open.spotify.com) - Music
* [Firefox](https://www.mozilla.org/en-US/firefox) - Browser  
  I know we use Gsuite and all, but I prefer Firefox
    
